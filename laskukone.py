def menu():
    print("Mitä haluat tehdä?")
    print("1. Laske yhteen")
    print("2. Vähennä")
    print("3. Kerro")
    print("4. Jaa")
    print("0. Lopeta")

tulos = 0

while True:
    print("Tulos on", tulos)
    menu()
    valinta = int(input("Valinta: "))
    if valinta == 0:
        break
    
    if valinta == 1:
        eka = int(input("Anna ensimmäinen luku: "))
        toka = int(input("Anna toinen luku: "))
        summa = eka + toka
        tulos = summa
    elif valinta == 2:
        eka = int(input("Anna ensimmäinen luku: "))
        toka = int(input("Anna toinen luku: "))
        erotus = eka - toka
        tulos = erotus
    elif valinta == 3:
        eka = int(input("Anna ensimmäinen luku: "))
        toka = int(input("Anna toinen luku: "))
        tulo = eka * toka
        tulos = tulo
    elif valinta == 4:
        eka = int(input("Anna ensimmäinen luku: "))
        toka = int(input("Anna toinen luku: "))
        osam = eka / toka
        tulos = osam
